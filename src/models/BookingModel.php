<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;
	private $dog;
	const BOOKINGS_ENTITY = 'bookings';

	function __construct() {
		$this->getBookingsData();
		$this->helper = new Helpers();
		$this->dog = new DogModel();
		$this->bookingData = $this->getBookingsData();
	}

	public function createBooking($data) {
		$data['id'] = end($this->bookingData)['id'] + 1;
		$dogs = $this->dog->getDogsByClientId($data['clientid']);

		$averageDogAge = $this->calculateDogsAverageAge($dogs);
		$data = $this->calculateDiscount($averageDogAge, $data);

		$this->bookingData[] = $data;
		try {
			$this->helper->putJson($this->bookingData, self::BOOKINGS_ENTITY);
			return $data;
		} catch (\Exception $e) {
			throw new \Exception('Error on creating a booking.');
		}
	}

	public function getBookings() {
		$this->bookingData = $this->getBookingsData();
		return $this->bookingData;
	}

	/**
	 * @return mixed
	 */
	public function getBookingsData() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		return json_decode($string, true);
	}

	/**
	 * @param array $dogs
	 * @return float|int
	 */
	public function calculateDogsAverageAge(array $dogs) {
		$totalAge = 0;
		foreach ($dogs as $dog) {
			$totalAge = +$dog['age'];
		}
		$averageDogAge = $totalAge / count($dogs);
		return $averageDogAge;
	}

	/**
	 * @param $averageDogAge
	 * @param $data
	 * @return mixed
	 */
	public function calculateDiscount($averageDogAge, $data) {
		if ($averageDogAge < 10) {
			$data['price'] = $data['price'] - ($data['price'] * 0.10);
		}
		return $data;
	}
}