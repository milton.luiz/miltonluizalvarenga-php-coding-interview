<?php

namespace Src\controllers;

use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking($data) {
		try {
			return $this->getBookingModel()->createBooking($data);
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}
}