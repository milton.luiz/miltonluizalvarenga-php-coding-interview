<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\controllers\Dog;

class BookingTest extends TestCase {

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function testGetBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

	public function testCreateBooking() {
		$booking = [
			"clientid" => 1,
			"price" => 299,
			"checkindate" => "2023-08-05 15:00:00",
			"checkoutdate" => "2023-08-08 15:00:00"
		];

		$result = $this->booking->createBooking($booking);
		$this->assertTrue($result, '');
	}

	public function testCreateBookingWithDiscount() {
		$request = [
			"clientid" => 1,
			"price" => 200,
			"checkindate" => "2023-09-05 15:00:00",
			"checkoutdate" => "2023-09-08 15:00:00"
		];
		$result = $this->booking->createBooking($request);

		$discount = $request['price'] - ($request['price'] * 0.10);

		$this->assertEquals($discount, $result['price']);
	}
}